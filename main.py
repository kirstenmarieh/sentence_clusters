import requests
import json
import numpy as np
from numpy import argsort
from nltk.tokenize import sent_tokenize
import pandas as pd
import spacy
from operator import itemgetter
from itertools import chain

def pub_dict_pubtator(res):
    """ Generated list of dictionaries for publications and bioconcepts from pubtator """
    # Iterate over publications

    pub_list = []
    for p in res:
        pub_di = {}
        if 'pmid' in p:

            # ### Id
            # pmid
            pub_di['id']={
                'pub_id': p['pmid'],
                'pub_id_type': 'pmid'
            }
            # ### Header
            pub_di['header']={}
            # year
            if 'year' in p['passages'][0]['infons']:
                pub_di['header']['pub_year'] = p['passages'][0]['infons']['year']
            # authors
            if 'authors' in p:
                pub_di['header']['authors'] = p['authors']

            # title
            if ('type' in p['passages'][0]['infons']
                    and  p['passages'][0]['infons']['type'] == 'title'
                    and 'text' in p['passages'][0]):
                pub_di['header']['title'] = p['passages'][0]['text']

            # journal
            if 'journal' in p:
                pub_di['header']['journal'] = p['journal']

                # Journal info (includes doi)
            if 'journal_info' in p['passages'][0]['infons']:
                pub_di['header']['journal_info'] = p['passages'][0]['infons']['journal']

            # bioconcepts in title
            di_title = create_spacy_benchmark_data_from_pubtator_biocjson([p], parts=['title'])
            #print(f'Keys: {di_title}')
            if len(di_title):
                del di_title[pub_di['header']['title']]['pmid']
                pub_di['header']['bioconcepts'] = di_title[pub_di['header']['title']]
            # ### Abstract
            pub_di['abstract'] = {}
            di_abstract = create_spacy_benchmark_data_from_pubtator_biocjson([p], parts=['abstract'])
            di_sent = sentence_tokenize_benchmark_data(di_abstract, keep_sent_wo_bioc=True)
            # +++ b
            pub_di['abstract']
            # +++ e
            pub_di['abstract']={'sentences': list(di_sent.keys()),
                                'bioconcepts': {j:{kk:di_sent[k][kk] for kk in di_sent[k]
                                                   if kk!='pub_part' and kk!='pmid'}
                                                for j, k in enumerate(di_sent.keys())},
                                'labels': [i/len(di_sent.keys()) for i in range(1,len(di_sent.keys())+1)]}

        pub_list.append(pub_di)
    return pub_list#, check_list

def info_from_pubtator_api(pmid_list,
                           Bioconcept=""
                           ):
    """ Get info from pubtator API for list of PMIDs """
    # pmid_list to dict
    jso = {"pmids": [str(p).strip() for p in pmid_list]}

    # load bioconcepts
    if Bioconcept != "":
        jso["concepts"]=Bioconcept.split(",")

    # send request request to pubtator api
    formt='biocjson'
    r = requests.post(
        "https://www.ncbi.nlm.nih.gov/research/pubtator-api/publications/export/"+formt,
        json = jso)
    if r.status_code != 200 :
        print ("[Error]: HTTP code "+ str(r.status_code))
        res=[]
    else:
        # res = '['+r.text.replace("}\n{","},{")+']'
        res = [json.loads(s) for s in r.text.split("\n") if len(s)]
        # +++ b
        # +++ e
    return res

def sentence_tokenize_benchmark_data(di, keep_sent_wo_bioc = False, tokenizer=sent_tokenize):
    """ Tokenizes sentences in abstract. Returns dictionary with sentences as keys
    and entities labeled with pubtator bioconcept classes """
    di_sent = {}
    # Iterate over text keys
    for k in di:
        # Tokenize sentences
        sentences_tmp = sent_tokenize(k)
        # Iterate over the sentences
        for i, s in enumerate(sentences_tmp):
            idx = k.index(s)
            ent_vec = di[k]['entities']
            nam_vec = di[k]['bioconcept']
            id_vec = di[k]['bioconcept_identifier']
            # Entities and names of the sentence
            v = []
            n = []
            id_v = []
            for j, e in enumerate(ent_vec):
                if e[0]-idx >= 0 and e[1]-idx < len(s):
                    v.append((e[0]-idx, e[1]-idx, e[2]))
                    n.append(nam_vec[j])
                    id_v.append(id_vec[j])
                    if s[v[-1][0]:v[-1][1]]!=n[-1]:
                        print(f"Names not equal! {s[v[-1][0]:v[-1][1]]} not {n[-1]}.")
            if len(v) or keep_sent_wo_bioc:
                # Copy the other fields
                di_sent[s]={kk:d for kk, d in di[k].items()
                            if kk!='entities'
                            and kk!='bioconcept'
                            and kk!='bioconcept_identifier'}
                # Entities, names, part + sentence number
                di_sent[s]['pub_part']=di_sent[s]['pub_part']+'_'+str(i)
                if len(v):
                    di_sent[s]['entities']=v
                    di_sent[s]['bioconcept']=n
                    di_sent[s]['bioconcept_identifier']=id_v
    return di_sent


def create_spacy_benchmark_data_from_pubtator_biocjson(res, parts=['title','abstract'], replace_empty_identifiers=True):
    """ Extract annotated data from pubtator biocson list.
    Format: {"Hans lived in Paris": {"entities": [(0, 5, "PERSON"), (14, 19, "LOC")]}
    Can include title and abstract, title only, or abstract only.
    parts = ['title'] title only
    parts = ['abstract'] abstract only
    parts = ['title','abstract'] title and abstract (default)
    """
    di = {}
    if 'title' in parts and 'abstract' in parts:
        v_parts = [0,1]
        parts = ['title','abstract']
    elif 'title' in parts:
        v_parts = [0]
        parts = ['title']
    elif 'abstract' in parts:
        v_parts = [1]
        parts = ['abstract']
    else:
        raise Exception(f"Invalid input value parts={parts}")
    for r in res:
        if 'passages' in r:
            li = r['passages']
            if 'pmid' in r:
                pmid = r['pmid']
            else:
                pmid = 0
            for i in v_parts:
                offset = li[i]['offset']
                if 'annotations' in li[i]:
                    # part (title or abstract)
                    part = li[i]['infons']['type']
                    # annotations
                    ann = li[i]['annotations'] # List of dictionaries
                    # +++ b
                    # print(f"ann={ann}")
                    # +++ e
                    # locations
                    # List of list of dictionaries
                    # e.g. [{'offset': 0, 'length': 8}]
                    if len(ann):
                        en=[]
                        l1=[]
                        # Iterate over annotations
                        for a in ann:
                            loc = a['locations']
                            # +++ b
                            # print(f"loc={loc}")
                            # +++ e
                            # Iterate over locations
                            l1.extend([l['offset'] for l in loc])
                            en.extend([
                                (l['offset']-offset,l['offset']+l['length']-offset,
                                 a['infons']['type'],
                                 a['infons']['identifier'])
                                for l in loc])
                        # Order entities by location
                        ix = argsort(l1)
                        en_ordered = [en[i] for i in ix]
                        # Remove duplicates
                        en_unique=[]
                        id_unique=[]
                        for e in en_ordered:
                            if not e in en_unique:
                                en_unique.append(e[:3])
                                id_unique.append(e[3])
                        # Create output dictionary
                        cur_key = li[i]['text']
                        # Entities and identifiers
                        di[cur_key]={'entities': en_unique, 'bioconcept_identifier': id_unique}
                        # names of entities
                        name_vec = [cur_key[nen[0]:nen[1]] for nen in di[cur_key]['entities']]
                        di[cur_key]['bioconcept'] = name_vec
                        # replace empty identifiers
                        if replace_empty_identifiers:
                            # +++ b
                            # print(f"BEFORE: Identifiers (e.g. MeSH): {di[cur_key]['bioconcept_identifier']}")
                            # +++ e

                            di[cur_key]['bioconcept_identifier']=['NO_ID__'+name_vec[i] if e=='-' or e=='' or e is None or e is np.nan
                                                                  else e
                                                                  for i, e in enumerate(di[cur_key]['bioconcept_identifier'])]
                            # +++ b
                            # print(f"AFTER: Identifiers (e.g. MeSH): {di[cur_key]['bioconcept_identifier']}")
                            # +++ e
                        # part (title or abstract)
                        di[cur_key]['pub_part']=part
                        # pmid
                        di[cur_key]['pmid']=pmid
                    # +++ b todo
                    # Add abstract
                    # +++ e todo
    return di

def create_cluster_list(df):
    def get_sentences_in_clusters(df):
        '''takes a pandas DataFrame.
        Returns a list of central sentences, and a list of lists of child sentences
        each sentence with their respective date and PMID attached to them.'''

    cluster_list=[]
    cluster_sublist=[]
    counter = 0
    k=0 #sentence number
    for i in df.Sentence:
        if df.DistanceToCentral[counter] == 0:
            k=0
            cluster_list.append(cluster_sublist)
            cluster_sublist=[]
            cluster_sublist.append((str(df.pmid[counter]), k))
        else:
            k+=1
            cluster_sublist.append((str(df.pmid[counter]), k))
        counter = counter + 1

    cluster_list.pop(0)
    cluster_list.append(cluster_sublist)
    return cluster_list


def get_pubrep_and_clusters():
    '''Uses the example sentence cluster result file and returns three things:
    1. the publication representation for all sentence PMIDs in the file, and
    2. A list of lists of tuples representative of each sentence cluster, [[(pmid_sent1, sentnum_sent1), (pmid_sent2, sentnum_sent2),...][(),()...]]
    3. A list of all sentences present in the file (search)'''

    example_cluster_fname ='/Users/Kirsten/Desktop/archimedes-system/configs/uiux/assets/mpn_melanoma_clustering_results_wo_comments.csv'#'configs/uiux/assets/mpn_melanoma_clustering_results_wo_comments.csv' #
    with open(example_cluster_fname, 'r') as f:
        df = pd.read_csv(f)

    pmid_list = []
    [pmid_list.append(str(pmid)) for pmid in df['PMID (Link)'] if pmid not in pmid_list] #create list of pmids to create pub rep

    pub_list = info_from_pubtator_api(pmid_list) #create pub rep
    pub_rep = pub_dict_pubtator(pub_list)

    cluster_list=[]
    cluster_sublist=[]
    counter = 0
    k=0 #sentence number
    for i in df.Sentence:
        if df.DistanceToCentral[counter] == 0:
            k=0
            cluster_list.append(cluster_sublist)
            cluster_sublist=[]
            cluster_sublist.append((str(df.pmid[counter]), k))
        else:
            k+=1
            cluster_sublist.append((str(df.pmid[counter]), k))
        counter = counter + 1

    cluster_list.pop(0)
    cluster_list.append(cluster_sublist)

    # cluster_list = []
    # sorted_df = df.sort_values(by='ClusterIndex') #sort the dataframe by cluster index (groups all sentences in same clusters)
    # sorted_df.to_csv('sorted_melanom.csv')
    # k = 0
    # cluster_sublist = []
    # for i, j in enumerate(sorted_df['ClusterIndex']): #traverse dataframe
    #     if i > 0 and sorted_df['ClusterIndex'].iloc[i] == sorted_df['ClusterIndex'].iloc[i-1]: # if sentences in same cluster
    #         cluster_tuple = (sorted_df['PMID (Link)'].iloc[i], k) #tuple of pmid and sentence number in cluster
    #         cluster_sublist.append(cluster_tuple) #append tuple to sublist
    #         k+=1
    #     else: #not in same cluster, start off with sentence number 0
    #         cluster_sublist = [] #new cluster, initialize empty list
    #         k = 0 #reset k to 0
    #         cluster_tuple = (sorted_df['PMID (Link)'].iloc[i], k) #tuple of pmid and sentence number in cluster
    #         cluster_sublist.append(cluster_tuple) #append tuple to sublist
    #         k+=1
    #     if sorted_df['ClusterIndex'].iloc[i] != sorted_df['ClusterIndex'].iloc[i-1]: #if cluster is finished
    #         cluster_list.append(cluster_sublist) #append sublist to grand list
    # #sentences = [sorted_df['Sentence'].iloc[i] for i in range(len(sorted_df['Sentence']))] #create list of all sentences in dataframe
    print(cluster_list)
    return pub_rep, cluster_list#, sentences

def cluster_fx(pub_rep, cluster_list):
    '''Creates a nested list of bioconcepts appearing in each cluster [[conc1cluster1, conc2cluster1,....],[conc1cluster2, conc2cluster2,...]...]
    Takes in the publication representation and cluster list. Creates the concept list of concept IDs and a list of concept strings occurring in each cluster.
    Calls calc_percentages to calculate the percentage a bioconcept occurs in each cluster as well as the percentage it occurs
    over all clusters. Calls DepTree with the bioconcept list as an argument to generate a list of lists of parts of speech per
    sentence cluster. Returns a dictionary of cluster_list, words, perc_occ_in_cluster, perc_occ_in_clusters, and pos.'''
    bioconcept_list = [] # list of all concepts occurring in the clusters
    conc_list = [] # list for concept string occurring in all clusters
    for cluster in cluster_list:
        bioconcept_sublist = []
        conc_str_sublist = []
        for tup in cluster: # indiv tuples
            for entry in pub_rep:
                if str(entry['id']['pub_id']) == tup[0]: #pmids are a match, get bioconcepts
                    try:
                        bioconcept_sublist = bioconcept_sublist + entry['abstract']['bioconcepts'][tup[1]]['bioconcept_identifier']
                        conc_str_sublist = conc_str_sublist + entry['abstract']['bioconcepts'][tup[1]]['bioconcept']
                    except KeyError:
                        continue
        conc_list.append(conc_str_sublist) #list of lists of concept strings
        bioconcept_list.append(bioconcept_sublist) #list of lists of concepts

    perc_per_cluster, perc_per_all = calc_percentages(cluster_list, bioconcept_list) # get percentage per cluster and percentage for all clusters
    percpercluster_list = []
    for entry in perc_per_cluster: #get top 10 words per cluster and add to list
        percpercluster_list.append(list(sorted(entry.items(), key=itemgetter(1), reverse=True)[:10])) #nested list of percentages in order from largest to smallest (max top 10 only)
    print(percpercluster_list)


    # new_percall_list = [] #this is for the percentages in all clusters by cluster
    # for entry in percpercluster_list:#sorted(entry.items(), key=itemgetter(1), reverse=True)[:10])#percpercluster_list: #traverse
    #     #print(sorted(entry.items(), key=itemgetter(1), reverse=True)[:10])
    #     #new_percall_dict = {}
    #     new_percall_sublist=[]
    #     for tup in entry:
    #         if tup[0] in perc_per_all.keys():
    #             new_percall_sublist.append(perc_per_all[tup[0]])
    #         #for k, v in perc_per_all.items():
    #         #a dictionary for clusters only
    #             #if k in #entry.keys(): #if ids match take key of entry and pair with value in percall_new_dict
    #               #  new_percall_dict[k]=v
    #     new_percall_list.append(new_percall_sublist)
    # print()
    # print('new perc list ', new_percall_list)
    # print()

    di = {'clusters': cluster_list,
          'words': [],
          'conc_id': [],
          'perc_occ_in_cluster': [],
          'perc_occ_in_all': [],
          'pos': [],
          'entities':[],
          'perc_in_all_by_cluster':[]
          }

    conc_dict = create_concdict(pub_rep) #dictionary {bioconcept text: id}.. traverse dict with typ[0] and get the string attached to it
    entity_dict = create_type_dict(pub_rep, conc_dict)
    word_list = [] #list for concept strings
    entity_list=[]
    new_percall_list = [] #this is for the percentages in all clusters by cluster
    for entry in percpercluster_list:
        perc_occ = [] # store percent occurrences
        word_sublist = [] # store concept strings
        entity_sublist=[]
        new_percall_sublist=[]
        for tup in entry:
            perc_occ.append(tup[1]) #tup[1] = percent occurs
            #id.append(tup[0]) #tup[0] is concept id
            if tup[0] in perc_per_all.keys():
                di['perc_occ_in_all'].append(perc_per_all[tup[0]]) #append the value associated with the key
            for k, v in conc_dict.items(): # create a nested list of concept strings
                if tup[0] == v:
                    word_sublist.append(k) # add the string to the list
            for k, v in entity_dict.items():
                if tup[0] == k:
                    entity_sublist.append(v)
            if tup[0] in perc_per_all.keys():
                new_percall_sublist.append(perc_per_all[tup[0]])
        entity_list.append(entity_sublist)
        word_list.append(word_sublist) # add to big list
        new_percall_list.append(new_percall_sublist)

        di['perc_occ_in_cluster'].append(perc_occ) # this is a nested list? (per cluster)
        #di['conc_id'].append(id) # add ids to dictionary
        di['words']=word_list
        di['entities']=entity_list
        di['perc_in_all_by_cluster'] = new_percall_list

    flat_list = [item for sublist in word_list for item in sublist] #flatten the nested list of concept strings
    for l in word_list:
        sub = []
        for c in l:
            sub.append('PROPN')
        di['pos'].append(sub)
   # pos_list = DepTree_wo_pmid(flat_list) #feed to nlp
   # for d in pos_list:
      #  for v in d.values():
         #   di['pos']+= v #add to dictionary
    print(di)
    return di

def create_type_dict(pub_rep, conc_dict):
    '''creates and returns a dictionary with bioconcept strings as keys and bioconcept entity type as values. Takes in the publication representation and the
    dictionary with bioconcept ids as values and bioconcept strings as keys.'''
    entity_di={}
    for key in conc_dict.keys():
        target = key #target bioconcept string
        for entry in pub_rep:
            for k in entry['abstract']['bioconcepts']:
                try:
                    for j in range(len(entry['abstract']['bioconcepts'][k]['bioconcept'])):
                        if entry['abstract']['bioconcepts'][k]['bioconcept'][j]==target: #if concepts match
                            entity_di[entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]]=entry['abstract']['bioconcepts'][k]['entities'][j][2]
                except KeyError:
                    continue
    return entity_di

def create_concdict(pub_rep):
    '''creates and returns a dictionary with bioconcept id's as keys and bioconcept strings as values. Takes in the publication representation.'''

    conc_di = {}
    entity_di={}
    for entry in pub_rep:
        for k in entry['abstract']['bioconcepts']:
            try:
                for j in range(len(entry['abstract']['bioconcepts'][k]['bioconcept'])):
                    conc_di[entry['abstract']['bioconcepts'][k]['bioconcept'][j]] =entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]
                    #entity_di[entry['abstract']['bioconcepts'][k]['bioconcept'][j]]=entry['abstract']['bioconcepts'][k]['entities'][j][2]
                    #conc_di[entry['abstract']['bioconcepts'][k]['bioconcept'][j]].append(entry['abstract']['bioconcepts'][k]['bioconcept_identifier'][j]) # entry['abstract']
                    #conc_di[entry['abstract']['bioconcepts'][k]['bioconcept'][j]].append(entry['abstract']['bioconcepts'][k]['entities'][j][2])
            except KeyError:
                continue
    new_di = {} # intermediate dictionary to remove duplicate values and keep only the one with shortest length
    for key, val in conc_di.items():
        new_di.setdefault(val, set()).add(key)

    for key, val in new_di.items():
        if len(val) > 1: #duplicate values exist
            temp_list = list(val) # turn sets to lists
            new_di[key] = min(temp_list, key=len) # find the minumum and replace the values with it
        else:
            new_di[key] = ''.join(val) # if length was 1, turn the set to a string
    conc_dict = {value: key for (key, value) in new_di.items()} # invert the dictionary
    #print(conc_dict)
    return conc_dict

#sentence cluster file does not contain all sentences in the pub rep... does it take into account titles??

def DepTree_wo_pmid(sentences):
    '''Takes in a list of sentences and identifies the parts of speech present. Returns a list of dictionaries of parts of speech,
    [{'pos: 'NOUN', 'ADJ',....},{'pos': 'VERB', 'PROPN',...}]'''
    model = "en_core_web_sm"
    # Load spacy model
    nlp = spacy.load(model)
    li = []
    i = 0
    for s in sentences:
        di = dict()
        d = nlp(s)
        i+=1
        djo = d.to_json()
        for t in d:
            di['pos']=[]
            r = 0
        for t in djo['tokens']:
            di['pos'].append(t['pos'])
        li.append(di)
    return li

def calc_percentages(cluster_list, bioconcept_list):
    '''calculates the percentage that a bioconcept appears in an individual sentence cluster. Takes the cluster list and bioconcept lists as input and returns
    a list of dictionaries with bioconcept identifiers as keys and percentage of appearance as values. Each dictionary corresponds to one cluster.'''
    percpercluster_list = [] #list to hold percentages referring to individual sentence clusters

    k=0
    for conc in bioconcept_list: #calculate percent of time each bioconcept occurs per cluster
        count_di = {i:round(conc.count(i)/len(cluster_list[k])*100) for i in conc}#/len(conc)
        percpercluster_list.append(count_di)
       # perall_di = {i:round(100-(v/len(cluster_list)*100)) for i in conc}
        k+=1

        #tot num clusters occurs in/tot num clusters
    #percall_list = list(chain.from_iterable(bioconcept_list)) #flattened list of all bioconcepts
    percall_new_dict = {} #{bioconcept id: num clusters occurs in}

    big_list = []
    for entry in bioconcept_list: #eliminate duplicates and count num occ
        big_list.append(list(set(entry)))

    for j in range(len(big_list)): #for each list, traverse and make key if not in list and assign value to 1, then continue to next entry
        for i in range(len(big_list[j])):
            target=big_list[j][i] #this is the concept to insert into dictionary
            if target in percall_new_dict.keys():
                percall_new_dict[big_list[j][i]]+=1
            else:
                percall_new_dict[big_list[j][i]]=1
    for k, v in percall_new_dict.items():
        percall_new_dict[k] = round(100-(v/len(cluster_list)*100)) #all percentages, connect to bioconcepts in cluster fx
    print(percall_new_dict)

    new_percall_list = []
    for entry in percpercluster_list: #traverse
        new_percall_dict = {}
        for k, v in percall_new_dict.items():
             #a dictionary for clusters only
            if k in entry.keys(): #if ids match take key of entry and pair with value in percall_new_dict
                new_percall_dict[k]=v
        new_percall_list.append(new_percall_dict)
    for i in range(len(new_percall_list)):
        new_percall_list[i] = {k: v for k, v in sorted(new_percall_list[i].items(), key=lambda item: item[1])}

    newt_percall_list=[]
    for ls in new_percall_list: #get top 10 and sort
        newt_percall_list.append(list(sorted(ls.items(), key=itemgetter(1), reverse=True)[:10]))
    print(newt_percall_list)
    return percpercluster_list, percall_new_dict


import plotly.express as px
if __name__== '__main__':
    test_val = 95
    color_continuous_scale = 'Turbo'
    #print(px.colors.colorscale_to_colors('Turbo'))
    example_cluster_fname ='/Users/Kirsten/Desktop/archimedes-system/configs/uiux/assets/mpn_melanoma_clustering_results_wo_comments.csv'#'configs/uiux/assets/mpn_melanoma_clustering_results_wo_comments.csv' #
    with open(example_cluster_fname, 'r') as f:
        df = pd.read_csv(f)
    create_cluster_list(df)
    pub_rep, cluster_list = get_pubrep_and_clusters()
    cluster_fx(pub_rep, cluster_list)
